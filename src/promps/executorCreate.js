import { prompt } from "inquirer";
import externalip from "externalip";
import { promisify } from "bluebird";
import { executor as executorApi } from "../api";
import store from "../store";

const externalIp = promisify(externalip);

export default () =>
    prompt([
        {
            name    : "name",
            type    : "input",
            message : "Enter a name of computer: "
        },
        {
            name    : "email",
            type    : "email",
            message : "Enter a email: "
        }
    ]).then(async ({ name, email }) => {
        const ip = await externalIp();
        const executor = await executorApi.create(store.room.id, { name, email, ip });

        if (executor) {
            store.setExecutor(executor);
        }

        return executor;
    });
