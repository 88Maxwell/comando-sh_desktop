import { prompt } from "inquirer";
import { room as roomApi } from "../api";
import store from "../store";

export default () =>
    prompt([
        {
            name    : "name",
            type    : "input",
            message : "Enter name of room: "
        },
        {
            name    : "password",
            type    : "password",
            message : "Enter a password: ",
            mask    : "*"
        }
    ]).then(async ({ name, password }) => {
        const room = await roomApi.login({ name, password });

        if (room) {
            store.setRoom(room);
        }

        return room;
    });
