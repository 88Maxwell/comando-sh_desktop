import { prompt } from "inquirer";
import { executor as executorApi } from "../api";
import store from "../store";
import spinner from "../spinner";
import scriptPolling from "../scriptPolling";

export default () =>
    prompt([
        {
            name    : "isConfirmed",
            type    : "confirm",
            message : "Do you confirm data usage? "
        }
    ]).then(async ({ isConfirmed }) => {
        if (!isConfirmed) return false;
        const { executor, room, setExecutor } = store;
        const newExecutorData = await executorApi.update(room.id, executor.id, { isConnected: true });

        if (!newExecutorData) return false;
        setExecutor(newExecutorData);
        scriptPolling.startPolling();
        spinner.start();

        return newExecutorData;
    });
