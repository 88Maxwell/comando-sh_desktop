import login from "./promps/login";
import executorCreate from "./promps/executorCreate";
import confirmDataUsing from "./promps/confirmDataUsing";

export default async () => {
    let answer = await login();

    if (answer) answer = await executorCreate();
    if (answer) answer = await confirmDataUsing();
};

