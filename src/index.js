import bluebird from "bluebird";
import scenaries from "./scenaries";
import exitHandler from "./onExit";

global.Promise = bluebird;

[
    "SIGHUP",
    "SIGINT",
    "SIGQUIT",
    "SIGILL",
    "SIGTRAP",
    "SIGABRT",
    "SIGBUS",
    "SIGFPE",
    "SIGUSR1",
    "SIGSEGV",
    "SIGUSR2",
    "SIGTERM"
].forEach(signalName => process.on(signalName, () => exitHandler(signalName)));

scenaries();
