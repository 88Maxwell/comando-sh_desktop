export default new class {
    constructor() {
        this.executor = null;
        this.room = null;
    }

    setRoom(room) {
        this.room = room;
    }

    setExecutor = (executor) => {
        this.executor = executor;
    }
}();

