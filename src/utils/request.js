import axios from "axios";
import config from "../../configs/config.json";

class API {
    constructor({ prefix = "api/v1" } = {}) {
        this.prefix = prefix;
        this.token = "";
        [ "get", "post", "patch", "delete" ].forEach(
            method =>
                (this[method] = async (url, data) =>
                    this.request({
                        url,
                        method : method.toUpperCase(),
                        body   : { data }
                    }))
        );
    }

    async request({ url, method, body }) {
        const { data: { data, status, error } } = await axios({
            url     : `${config.apiUrl}/${this.prefix}${url}`,
            method,
            headers : {
                "Content-Type"  : "application/json",
                "Cache-Control" : "no-cache",
                pragma          : "no-cache",
                ...(this.token ? { "X-AuthToken": this.token } : {})
            },
            withCredentials : true,
            crossDomain     : false,
            data            : method !== "GET" ? JSON.stringify(body) : undefined
        });

        if (status !== 200) console.error("error: ", error);

        return data;
    }

    setToken(token) {
        this.token = token;
    }
}

export default new API({ prefix: config.apiPrefix });
