import request from "./utils/request";

export const room = {
    login : data => request.post("/roomLogin", data)
};

export const executor = {
    create : (roomId, data) => request.post(`/user/room/${roomId}/executor`, data),
    update : (roomId, executorId, data) => request.patch(`/user/room/${roomId}/executor/${executorId}`, data)
};

export const script = {
    show   : executorId => request.get(`/executor/${executorId}/script`),
    update : (scriptId, data) => request.patch(`/executor/script/${scriptId}`, data)
};

