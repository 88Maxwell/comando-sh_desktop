import { exec } from "shelljs";

import config from "../configs/config";
import store from "./store";
import { script as scriptApi } from "./api";

export default new (class {
    constructor() {
        this.id = "";
    }

    startPolling() {
        this.id = setInterval(async () => {
            const script = await scriptApi.show(store.executor.id);

            if (Object.keys(script).length) {
                const result = this.runScript(script.data);
                const status = !result.status ? "SUCCESSFUL" : "FAILED";

                return scriptApi.update(script.id, { status });
            }
        }, config.pollingInterval);
    }

    stopPolling() {
        clearInterval(this.id);
    }

    runScript(data) {
        return exec(data, { timeout: 4000 });
    }
})();
