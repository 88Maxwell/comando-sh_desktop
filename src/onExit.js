import store from "./store";
import scriptPolling from "./scriptPolling";
import { executor as executorApi } from "./api";

export default signalName => {
    if (typeof signalName === "string") disconnect(() => process.exit(1));
    console.log("Goodbuy !!!");
};


async function disconnect(cb) {
    const { room, executor } = store;

    if (room && executor && room.id && executor.id) {
        scriptPolling.stopPolling();
        await executorApi.update(store.room.id, store.executor.id, { isConnected: false });
    }
    cb();
}
